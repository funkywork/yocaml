### dev (unreleased)

#### Core

- Complete reconstruction of the YOCaml core (by [**xhtmlboi**](https://github.com/xhtmlboi), [**xvw**](https://github.com/xvw))

### v1.0.0 2023-11-15 Paris (France)

- First release of YOCaml
